/* eslint-disable @typescript-eslint/no-explicit-any */
import { afterAll, beforeAll, describe, expect, test } from '@jest/globals';
import { until, WebDriver } from 'selenium-webdriver';
import { gridPage } from '../support/index';
import * as capabilities from '../support/capabilities.json';
import { DriverFactory } from '../support/driver-factory';

const capabilitiesMap = new Map(Object.entries(capabilities))
let userBrowser: WebDriver;
let userCaps: any;
describe('Selenium Grid Test', () => {

  beforeAll(async () => {
    userCaps = capabilitiesMap.get('hub');
    userBrowser = await DriverFactory.getBrowser(userCaps);
    await userBrowser.get('https://www.selenium.dev/documentation/grid/');
  });

  afterAll(async () => {
    await userBrowser.close();
    await userBrowser.quit();
  });

  test('should have the correct page header', async () => {
    const element = await userBrowser.findElement(gridPage.header).getText()
    expect(element).toEqual('Grid')
  });

  test('should click the component link text', async () => {
    await userBrowser.findElement(gridPage.componentsLinkText).click();
    expect(await userBrowser.wait(until.urlContains('components_of_a_grid'))).toBe(true);
  });

});