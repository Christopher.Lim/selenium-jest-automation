import { By } from "selenium-webdriver";

export default class GridPage {
  header = By.css('h1');
  componentsLinkText = By.css('body > div > div.td-main > div > main > div > div.section-index > div:nth-child(2) > h5 > a');
}

export const gridPage = new GridPage();